﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace JsonDemo.Model
{
    public class MessageResult<T>
    {
        public T Data { get; set; }
        public int Code { get; set; }
        public string Msg { get; set; }
    }
}
