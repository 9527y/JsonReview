﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDemo.Model
{
    public class Dog
    {
        public int Age { set; get; }
        public string Name { set; get; }
    }
}
