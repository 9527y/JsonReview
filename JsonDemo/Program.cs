﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using fastJSON;
using JsonDemo.Model;
using LitJson;
using Newtonsoft.Json;
using ServiceStack;

namespace JsonDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Dog> dogs = new List<Dog>();

            for (int i = 0; i < 100; i++)
            {
                dogs.Add(new Dog()
                {
                    Name = "Tom" + i,
                    Age = i
                });
            }


            Person person = new Person { Name = "Alex", Age = 18, Address = "北京市朝阳区", Pets = dogs };
            MessageResult<Person> data = new MessageResult<Person>();
            data.Code = 1;
            data.Msg = "Alex";
            data.Data = person;

            string jsonStr = person.ToJson();
            string dataStr = data.ToJson();
            Stopwatch sw = null;

            Console.WriteLine("*******序列化比较*******");
            for (int ii = 0; ii < 3; ii++)
            {
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    person.ToJson();
                }
                sw.Stop();

                Console.WriteLine("========={0}===========", ii);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ServiceStack.Text:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonConvert.SerializeObject(person);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("JsonConvert.SerializeObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JSON.ToJSON(person);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("fastJSON.ToJSON:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonMapper.ToJson(person);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("JsonMapper.ToJson:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
            }

            //------------------//
            Console.WriteLine("*******反序列化比较*******");

            for (int ii = 0; ii < 3; ii++)
            {
                Console.WriteLine("========={0}===========", ii);
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    jsonStr.FromJson<Person>();
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ServiceStack.Text:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    jsonStr.FromJson<Person>();
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("JsonConvert.SerializeObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JSON.ToObject(jsonStr, typeof(Person));
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("fastJSON.ToJSON:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonMapper.ToObject<Person>(jsonStr);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("JsonMapper.ToObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
            }

            Console.WriteLine("★★★★★★★泛型★★★★★★★");
            Console.WriteLine("*******序列化比较*******");
            for (int ii = 0; ii < 3; ii++)
            {
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    data.ToJson();
                }
                sw.Stop();
                Console.WriteLine("========={0}===========", ii);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("ServiceStack.Text:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonConvert.SerializeObject(data);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("JsonConvert.SerializeObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("+++++++++++++++++++++");
                Thread.Sleep(4000);
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JSON.ToJSON(data);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("fastJSON.ToJSON:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonMapper.ToJson(data);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("JsonMapper.ToJson:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
            }

            //------------------//
            Console.WriteLine("*******反序列化比较*******");

            for (int ii = 0; ii < 3; ii++)
            {
                Console.WriteLine("========={0}===========", ii);
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    dataStr.FromJson<MessageResult<Person>>();
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("ServiceStack.Text:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    dataStr.FromJson<MessageResult<Person>>();
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("JsonConvert.SerializeObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JSON.ToObject(dataStr, typeof(MessageResult<Person>));
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("fastJSON.ToJSON:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
                Console.WriteLine("+++++++++++++++++++++");
                sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 10000; i++)
                {
                    JsonMapper.ToObject<MessageResult<Person>>(dataStr);
                }
                sw.Stop();
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("JsonMapper.ToObject:{0}", sw.Elapsed);
                Console.ForegroundColor = ConsoleColor.White;
                Thread.Sleep(4000);
            }


            Console.Read();
        }
    }
}
